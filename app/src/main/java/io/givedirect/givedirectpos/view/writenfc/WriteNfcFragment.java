package io.givedirect.givedirectpos.view.writenfc;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.OnClick;
import io.givedirect.givedirectpos.R;
import io.givedirect.givedirectpos.model.util.NfcUtil;
import io.givedirect.givedirectpos.presenter.writenfc.WriteNfcContract;
import io.givedirect.givedirectpos.presenter.writenfc.WriteNfcFlowManager;
import io.givedirect.givedirectpos.view.common.BaseViewFragment;
import io.givedirect.givedirectpos.view.util.TextUtils;

public class WriteNfcFragment extends BaseViewFragment<WriteNfcContract.WriteNfcPresenter>
        implements WriteNfcContract.WriteNfcView {

    @BindView(R.id.write_success_container)
    ViewGroup successContainer;

    @BindView(R.id.write_success_pra)
    TextView writtenPraId;

    @BindView(R.id.write_success_address)
    TextView writtenAddress;

    @BindView(R.id.wristband_contents_container)
    ViewGroup contentsContainer;

    @BindView(R.id.read_pra)
    TextView readPraId;

    @BindView(R.id.read_address)
    TextView readAddress;

    @BindView(R.id.contents_matched_message)
    TextView contentsMatchedMessage;

    @BindView(R.id.overwrite_button)
    Button overwriteButton;

    @BindView(R.id.scan_again_message)
    TextView scanAgainMessage;

    @BindView(R.id.write_failure_message)
    TextView failureMessage;

    public static WriteNfcFragment newInstance() {
        return new WriteNfcFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.write_nfc_fragment, container, false);
    }

    @Override
    public void showLoading(boolean isLoading) {
        ((WriteNfcFlowManager) activityContext).showLoading(isLoading);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        successContainer.setVisibility(View.GONE);
        contentsContainer.setVisibility(View.GONE);
        failureMessage.setVisibility(View.GONE);
    }

    @Override
    public void writeWristband(@NonNull String encryptedSeed,
                               @NonNull String publicAddress) {
        boolean successfulWrite = NfcUtil.bindNFCMessage(encryptedSeed,
                ((Activity) activityContext).getIntent());
        if (successfulWrite) {
            presenter.onWristbandWritten(publicAddress);
        } else {
            presenter.onWristbandWriteFailure();
        }
    }

    @Override
    public void showWriteFailure() {
        successContainer.setVisibility(View.GONE);
        contentsContainer.setVisibility(View.GONE);
        failureMessage.setVisibility(View.VISIBLE);
    }

    @Override
    public void showWriteSuccess(@NonNull String publicAddress) {
        contentsContainer.setVisibility(View.GONE);
        failureMessage.setVisibility(View.GONE);
        writtenPraId.setText(getString(R.string.pra_title, NfcUtil.getPraId(publicAddress)));
        writtenAddress.setText(publicAddress);
        successContainer.setVisibility(View.VISIBLE);
    }

    @Override
    public void showWristbandContents(@NonNull String publicAddress,
                                      @Nullable String lastWrittenAddress,
                                      boolean isMatchFromLastWrite) {
        failureMessage.setVisibility(View.GONE);
        if (!TextUtils.isEmpty(lastWrittenAddress)) {
            writtenPraId.setText(getString(R.string.pra_title, NfcUtil.getPraId(lastWrittenAddress)));
            writtenAddress.setText(lastWrittenAddress);
            successContainer.setVisibility(View.VISIBLE);
            contentsMatchedMessage.setVisibility(View.VISIBLE);
        } else {
            successContainer.setVisibility(View.GONE);
            contentsMatchedMessage.setVisibility(View.GONE);
        }

        scanAgainMessage.setVisibility(View.GONE);
        readPraId.setText(getString(R.string.pra_title, NfcUtil.getPraId(publicAddress)));
        readAddress.setText(publicAddress);
        contentsMatchedMessage.setText(getString(isMatchFromLastWrite ?
                R.string.wristband_contents_matched_last_write
                : R.string.wristband_contents_mismatch));
        contentsMatchedMessage.setTextColor(ContextCompat.getColor(activityContext,
                isMatchFromLastWrite ? R.color.positiveColor : R.color.warningColor));
        overwriteButton.setText(R.string.overwrite_button);
        overwriteButton.setVisibility(View.VISIBLE);
        contentsContainer.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.overwrite_button)
    public void onOverwriteRequested() {
        scanAgainMessage.setVisibility(View.VISIBLE);
        overwriteButton.setVisibility(View.GONE);
        presenter.onUserRequestWristbandOverwrite();
    }

    @Override
    public void showWristbandReadFailure() {
        Toast.makeText(activityContext, getString(R.string.nfc_parse_error),
                Toast.LENGTH_SHORT).show();
    }

    @Nullable
    @Override
    public NfcUtil.GiveDirectNFCWallet parseNFC(@NonNull String authSeed) {
        return NfcUtil.parseNfcIntent(getActivity().getIntent(), authSeed);
    }

    public void onNFCScanned() {
        presenter.onNfcScanned();
    }
}
