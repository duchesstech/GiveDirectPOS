package io.givedirect.givedirectpos.presenter.writenfc;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import org.stellar.sdk.KeyPair;

import io.givedirect.givedirectpos.dagger.SchedulerProvider;
import io.givedirect.givedirectpos.model.repository.AppFlagRepository;
import io.givedirect.givedirectpos.model.util.NfcUtil;
import io.givedirect.givedirectpos.model.util.NfcUtil.GiveDirectNFCWallet;
import io.givedirect.givedirectpos.presenter.common.BasePresenter;
import timber.log.Timber;

public class WriteNfcPresenter extends BasePresenter<WriteNfcContract.WriteNfcView>
        implements WriteNfcContract.WriteNfcPresenter {
    private static final String UNRECOGNIZED_CONTENTS = "UNRECOGNIZED";

    @NonNull
    private final AppFlagRepository appFlagRepository;

    @NonNull
    private final SchedulerProvider schedulerProvider;

    @Nullable
    private String lastWrittenPublicAddress;

    @Nullable
    private String lastReadPublicAddress;

    private boolean isReadingBand = false;
    private boolean isGeneratingSeed = false;
    private boolean isPendingOverwrite = false;

    protected WriteNfcPresenter(@NonNull WriteNfcContract.WriteNfcView view,
                                @NonNull AppFlagRepository appFlagRepository,
                                @NonNull SchedulerProvider schedulerProvider) {
        super(view);
        this.appFlagRepository = appFlagRepository;
        this.schedulerProvider = schedulerProvider;
    }

    @Override
    public void onNfcScanned() {
        checkWristbandContents();
    }

    @SuppressLint("CheckResult")
    private void checkWristbandContents() {
        if (isReadingBand || isGeneratingSeed) {
            return;
        }

        isReadingBand = true;

        appFlagRepository.getAuthSeed()
                .compose(schedulerProvider.singleScheduler())
                .doOnSubscribe(disposable -> {
                    addDisposable(disposable);
                    view.showLoading(true);
                })
                .doAfterTerminate(() -> {
                    view.showLoading(false);
                    isReadingBand = false;
                })
                .subscribe(
                        authSeed ->
                                handleWristbandContentRead(view.parseNFC(authSeed)),
                        throwable -> {
                            Timber.e(throwable, "Failed to retrieve auth");
                            view.showWristbandReadFailure();
                        });
    }

    private void handleWristbandContentRead(@Nullable GiveDirectNFCWallet wallet) {
        if (wallet == null) {
            return;
        }

        if (wallet.isValidFormat() || !wallet.isEmpty()) {
            // Wristband contents are not empty.
            String pubAddress = wallet.getPublicKey() == null ? UNRECOGNIZED_CONTENTS
                    : wallet.getPublicKey();
            if (isPendingOverwrite && pubAddress.equals(lastReadPublicAddress)) {
                attemptWristbandWrite();
            } else {
                lastReadPublicAddress = pubAddress;
                isPendingOverwrite = false;
            }

            view.showWristbandContents(lastReadPublicAddress,
                    lastWrittenPublicAddress,
                    lastWrittenPublicAddress != null
                            && lastWrittenPublicAddress.equals(lastReadPublicAddress));
        } else {
            // Wristband contents are empty, good to go straight to writing.
            attemptWristbandWrite();
        }
    }

    @Override
    public void onWristbandWriteFailure() {
        lastWrittenPublicAddress = null;
        lastReadPublicAddress = null;
        isPendingOverwrite = false;
        view.showWriteFailure();
    }

    @Override
    public void onWristbandWritten(@NonNull String publicAddress) {
        lastWrittenPublicAddress = publicAddress;
        view.showWriteSuccess(publicAddress);
    }

    @Override
    public void onUserRequestWristbandOverwrite() {
        isPendingOverwrite = true;
    }

    @SuppressLint("CheckResult")
    private void attemptWristbandWrite() {
        if (isGeneratingSeed) {
            return;
        }

        isGeneratingSeed = true;

        appFlagRepository.getAuthSeed()
                .compose(schedulerProvider.singleScheduler())
                .doOnSubscribe(disposable -> {
                    addDisposable(disposable);
                    view.showLoading(true);
                })
                .doAfterTerminate(() -> {
                    view.showLoading(false);
                    isGeneratingSeed = false;
                    isPendingOverwrite = false;
                })
                .subscribe(authSeed -> {
                    KeyPair keyPair = KeyPair.random();
                    String encryptedSeed = NfcUtil.encryptSeed(new String(keyPair.getSecretSeed()),
                            authSeed);
                    view.writeWristband(encryptedSeed, keyPair.getAccountId());
                }, throwable -> {
                    Timber.e(throwable, "Failed to generate private key");
                    view.showWriteFailure();
                });
    }
}
