package io.givedirect.givedirectpos.presenter.writenfc;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import io.givedirect.givedirectpos.model.util.NfcUtil;
import io.givedirect.givedirectpos.presenter.common.Presenter;

public interface WriteNfcContract {
    interface WriteNfcView {
        void showLoading(boolean isLoading);

        void writeWristband(@NonNull String encryptedSeed,
                            @NonNull String publicAddress);

        void showWriteFailure();

        void showWriteSuccess(@NonNull String publicAddress);

        void showWristbandContents(@NonNull String publicAddress,
                                   @Nullable String lastWrittenAddress,
                                   boolean isMatchFromLastWrite);

        void showWristbandReadFailure();

        @Nullable
        NfcUtil.GiveDirectNFCWallet parseNFC(@NonNull String authSeed);
    }

    interface WriteNfcPresenter extends Presenter {
        void onNfcScanned();
        void onWristbandWriteFailure();
        void onWristbandWritten(@NonNull String publicAddress);
        void onUserRequestWristbandOverwrite();
    }
}
